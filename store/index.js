export const state = () => ({
  activeSidebar: false,
  activeLoginDialog: false,
  isAuthenticated: false,
  loggedInUser: null,
})

export const mutations = {
  set_activeSidebar(state, newActiveSidebar) {
    state.activeSidebar = newActiveSidebar
  },
  set_activeLoginDialog(state, newActiveLoginDialog) {
    state.activeLoginDialog = newActiveLoginDialog
  },
  set_isAuthenticated(state, newIsAuthenticated) {
    state.isAuthenticated = newIsAuthenticated
  },
  set_loggedInUser(state, newLoggedInUser) {
    state.loggedInUser = newLoggedInUser
  },
}