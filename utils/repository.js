import { request } from './api'

const HOSTNAME = '127.0.0.1:8000'
export { getSellRequest }

/***
 * 
 *  API REPOSITORY
 * 
 */

/****
 * GET
 */
function getSellRequest(userId) {
  const url = `http://${HOSTNAME}/api/v1/sellrequests-management/sellrequests?user_id=${userId}`
  return request('get',url, {}, true)
}

/****
 * POST
 */

/****
 * PUT
 */

/****
 * DELETE
 */