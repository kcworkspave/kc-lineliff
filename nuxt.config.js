export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'kc-lineliff',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'vuesax/dist/vuesax.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/vuesax'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://auth.nuxtjs.org/
    '@nuxtjs/auth-next',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // With options
    ['cookie-universal-nuxt', { alias: 'cookiz' }],
  ],

  // Auth-nuxt
  auth: {
    redirect: {
      login: '/login'
    },
    strategies: {
      local: {
        tokenName: 'Authorization',
        token: {
          property: 'data.api_token',
          type: 'Bearer',
          // required: true,
        },
        user: {
          property: false, // here should be `false`, as you defined in user endpoint `propertyName`
          autoFetch: true
        },
        endpoints: {
          login: {
            url: 'http://127.0.0.1:8000/api/v1/auth-management/get-token', 
            method: 'post',
          },
          logout: {
            url: 'http://127.0.0.1:8000/api/v1/auth-management/logout', 
            method: 'get',
          },
          user: {
            url: `http://127.0.0.1:8000/api/v1/auth-management/user`, 
            method: 'get', 
            propertyName: 'data'
          },
        }
      }
    }
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

}
